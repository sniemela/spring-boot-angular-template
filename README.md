# Spring Boot and Angular template

This is a maven based project template that comes with Spring Boot and Angular.

## Requirements

* Java 11

## Build

Execute the following command:

    ./mvnw clean package

This will produce a single JAR. The Angular application is packaged to the final JAR as a WebJAR.

Start the app:

    java -jar backend/target/*.jar

Visit http://localhost:8080. Have fun!